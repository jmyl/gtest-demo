# gtest-demo

```bash
cmake -S. -Bbuild 
cmake --build build
```


## Running the tests

Either using `ctest`:
```
$ cd build
$ ctest

Running tests...
Test project /home/user/gtest-demo/build
    Start 1: unit
1/1 Test #1: unit .............................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   0.00 sec
```

Or directly using `unit_tests`:
```
$ cd build 
$ ./bin/unit_tests

[==========] Running 2 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 2 tests from example
[ RUN      ] example.add
[       OK ] example.add (0 ms)
[ RUN      ] example.subtract
[       OK ] example.subtract (0 ms)
[----------] 2 tests from example (1 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 1 test case ran. (1 ms total)
[  PASSED  ] 2 tests.

```

## Submit to GDash

```
$ cd build
$ ctest -D Experimental

   Site: jmyl-XPS-13-9380
   Build name: Linux-c++
Create new tag: 20210424-2159 - Experimental
Configure project
   Each . represents 1024 bytes of output
    . Size of output: 0K
Build project
   Each symbol represents 1024 bytes of output.
   '!' represents an error and '*' a warning.
    . Size of output: 0K
   0 Compiler errors
   0 Compiler warnings
Test project /home/jmyl/Projects/gtest-demo/build
    Start 1: example.add
1/2 Test #1: example.add ......................   Passed    0.00 sec
    Start 2: example.subtract
2/2 Test #2: example.subtract .................   Passed    0.00 sec

100% tests passed, 0 tests failed out of 2

Label Time Summary:
unit    =   0.00 sec*proc (2 tests)

Total Test time (real) =   0.00 sec
Performing coverage
   Processing coverage (each . represents one file):
    ...
   Accumulating results (each . represents one file):
    ......
        Covered LOC:         26
        Not covered LOC:     12
        Total LOC:           38
        Percentage Coverage: 68.42%
Submit files
   SubmitURL: http://my.cdash.org/submit.php?project=jmylExample
   Uploaded: /home/jmyl/Projects/gtest-demo/build/Testing/20210424-2159/Configure.xml
   Uploaded: /home/jmyl/Projects/gtest-demo/build/Testing/20210424-2159/Build.xml
   Uploaded: /home/jmyl/Projects/gtest-demo/build/Testing/20210424-2159/Test.xml
   Uploaded: /home/jmyl/Projects/gtest-demo/build/Testing/20210424-2159/Coverage.xml
   Uploaded: /home/jmyl/Projects/gtest-demo/build/Testing/20210424-2159/CoverageLog-0.xml
   Uploaded: /home/jmyl/Projects/gtest-demo/build/Testing/20210424-2159/Done.xml
   Submission successful
```


## Acknowledgments

- Container Travis setup thanks to [Joan Massich](https://github.com/massich).
- Clean-up in CMake code thanks to [Claus Klein](https://github.com/ClausKlein).
- Clean-up and GitHub Actions workflow: [Roberto Di Remigio](https://github.com/robertodr).
