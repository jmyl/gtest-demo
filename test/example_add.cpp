#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <filesystem>
#include <vector>
#include "example.h"

using ::testing::StartsWith;

std::vector<std::string> get_sub_folders(std::string cases) {
    std::vector<std::string> files;
    for (const auto & entry : std::filesystem::directory_iterator(cases)) {
        files.push_back(entry.path());
        std::cout << entry.path() << std::endl;
    }
    return files;
}

class Shot: public testing::TestWithParam<std::string> {};

TEST_P(Shot, add_r1) {
    std::string a = GetParam();
    EXPECT_THAT(a, StartsWith("/home/jmyl/"));
}

INSTANTIATE_TEST_SUITE_P(
        ShotTest, Shot, ::testing::ValuesIn(get_sub_folders("/home/jmyl/Documents/.reference_results/")));

TEST(example, add) {
    double res;
    res = add_numbers(1.0, 2.0);
    ASSERT_NEAR(res, 3.0, 1.0e-11);
}

class OperatorTestFixture: public testing::TestWithParam<int> {};

TEST_P(OperatorTestFixture, add_1) {
    double a = GetParam();
    ASSERT_EQ(a+1, add_numbers(a, 1));
}

TEST_P(OperatorTestFixture, add_2) {
    double a = GetParam();
    ASSERT_EQ(a+2, add_numbers(a, 2));
}

const int params[] = {-10, 1, 2, 3, 4, 500};

INSTANTIATE_TEST_SUITE_P(
        example, OperatorTestFixture, ::testing::ValuesIn(params));


class MyTestSuite : public testing::TestWithParam<int> {};

TEST_P(MyTestSuite, MyTest)
{   
    std::cout << "Example Test Param: " << GetParam() << std::endl;
}

INSTANTIATE_TEST_SUITE_P(MyGroup, MyTestSuite, testing::Range(0, 10),
        testing::PrintToStringParamName());

